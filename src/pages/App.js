import React, { Component } from 'react';
import StatusAll from './components/StatusAll'
import '../App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        </header>
          <StatusAll />
      </div>
    );
  }
}

export default App;
