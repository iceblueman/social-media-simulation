import React, { Component } from 'react';
import StatusList from './StatusList'
import './Status.css';

export default class StatusAll extends Component{

    constructor(props){
        super(props);
        this.state = {
            newStateText : "",
            statuses : [
                "Started",
                "Finished!",
            ]
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.delete = this.delete.bind(this);
    }

    delete(index){
        let newStatuses = [...this.state.statuses];
        newStatuses.splice(index,1);
        this.setState({
            statuses : newStatuses,
        });
    }

    handleChange(event){
        this.setState({newStateText: event.target.value})
    }

    handleSubmit(event){
        event.preventDefault();
        let newStatuses = [this.state.newStateText,...this.state.statuses];
        this.setState({
            newStateText : "",
            statuses : newStatuses
        })
    }

    render() {
        return(
            <div className={"all-statuses"}>
                <div className="form-status">
                    <h3 className={"app-name"}>Social Media Simulation</h3>
                    <form className={"add-form"} onSubmit={this.handleSubmit}>
                        <input className={"add-status-text"} type="text" value={this.state.newStateText} onChange={this.handleChange} placeholder={"What's on your mind?"}/>
                    </form>
                </div>
                <div className="all-statuses-list">
                    <StatusList statuses={this.state.statuses}
                                delete={this.delete}/>
                </div>
            </div>
        );
    }
}
