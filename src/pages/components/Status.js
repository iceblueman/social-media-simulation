import React, { Component } from 'react';
import {FaRegTrashAlt, FaThumbsUp, } from "react-icons/fa";
export default class Status extends Component{

    constructor(){
        super();
        this.state = {
            likes : 0
        }
        this.like = this.like.bind(this);
        this.delete = this.delete.bind(this);
    }

    like(){
        this.setState({
            likes : this.state.likes + 1
        })
    }

    delete(){
        this.props.delete(this.props.index);
    }

    plurify(text,number){
        return number>0? text+'s': text;
    }

    render() {
        return(
            <div className={"status"}>
                <div className={"close"} onClick={this.delete}>
                    <FaRegTrashAlt/>
                </div>
                <p>
                    {this.props.text}
                </p>
                <p>

                        {this.plurify('Like',this.state.likes)} <FaThumbsUp onClick={this.like}/>

                </p>
            </div>
        );
    }
}
